angular.module('menuApp', ['ngRoute'])  
.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/menu1', {
        templateUrl: 'pages/menu1.html',
        controller: 'menu_1_Controller'
      }).
      when('/menu2', {
        templateUrl: 'pages/menu2.html',
        controller: 'menu_2_Controller'
      }).
    when('/menu3', {
        templateUrl: 'pages/menu3.html',
        controller: 'menu_3_Controller'
      }).
      otherwise({
        redirectTo: '/',
         controller: 'homeController',
       templateUrl: 'pages/home.html'
      });
  }])

.controller('homeController', function($scope) {
     
    $scope.message = 'This is home';
     
})
.controller('menu_1_Controller', function($scope) {
     
    $scope.message = 'This is menu1';
     
})
.controller('menu_2_Controller', function($scope) {
     
    $scope.message = 'This is menu2';
     
})
.controller('menu_3_Controller', function($scope) {
     
    $scope.message = 'This is menu3';
     
});
 